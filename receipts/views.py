from django.shortcuts import render
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm
from django.shortcuts import redirect


def viewreceipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user.id)
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)


def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user.id)
    context = {"categories": categories}
    return render(request, "receipts/categories.html", context)


def account_list(request):
    account_lists = Account.objects.filter(owner=request.user.id)
    context = {"account_lists": account_lists}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"create_form": form}
    return render(request, "receipts/create.html", context)


def create_expense(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("list_categories")
    else:
        form = ExpenseForm()
    context = {"create_expense": form}
    return render(request, "receipts/createexpense.html", context)
