from django.urls import path
from receipts.views import (
    viewreceipts,
    create_receipt,
    category_list,
    account_list,
    create_expense,
)

urlpatterns = [
    path("", viewreceipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="list_categories"),
    path("accounts/", account_list, name="accounts_list"),
    path("categories/create/", create_expense, name="create_category"),
]
