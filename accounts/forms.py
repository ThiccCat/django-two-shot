from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, required=True)
    password = forms.CharField(
        max_length=150, widget=forms.PasswordInput, required=True
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150, required=True)
    password = forms.CharField(
        max_length=150, required=True, widget=forms.PasswordInput
    )
    password_confirmation = forms.CharField(
        max_length=150, required=True, widget=forms.PasswordInput
    )
